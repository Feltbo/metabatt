#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jun 16 14:26:06 2021

@author: felix

Just used to run some tests
"""


# %%
if __name__ == "__main__":
    import matplotlib.pyplot as plt
    import numpy as np
    import torch
    import torch.nn as nn
    import torch.optim as optim
    import torch.nn.functional as F
    import metabatt
    from metabatt.graph import Graph, Subgraphs
    from metabatt.grid import Grid
    from metabatt.reinforce.reinforce import MetabattEnvDiscrete,\
        MetaBattEnvDiscreteEdges
    from metabatt.reinforce.agents import QAgentAddSubgraphs,\
        QAgentReplayMemory
    from metabatt.reinforce.environments import MetabattEnvArrangeGrid,\
        MetabattEnvSubgraphs,\
        MetabattEnvSubgraphsDumbVersion
    from time import time

    # start by creating the node layout
    # graph = Graph(4, 4,
    #               max_edge_length=1.5,
    #               node_layout='uniform',
    #               remove_nodes_from_grid=[],
    #               allowed_states=[i for i in range(256)]) # [5, 6, 7]) # [str(i) for i in range(9)]
    # graph.populate_nodes()

    subgraphs = Subgraphs(angles=[0, 30, 60, 90, 120, 150, 180],
                          lengths=[1])

    # # create the environment
    # env = MetabattEnvSubgraphsDumbVersion(n_cells_x=3,
    #                            n_cells_y=3,
    #                            subgraphs=subgraphs)

    # agent = QAgentAddSubgraphs(env=env,
    #                           n_inputs=16, # this is the state representation
    #                           n_outputs=env.action_space.n,
    #                           epsilon=1,
    #                           gamma=0.99,
    #                           learning_rate=0.001,
    #                           random_actions=True) # random agent

    env = MetabattEnvSubgraphs(n_cells_x=3,
                               n_cells_y=3,
                               subgraphs=subgraphs)

    agent = QAgentReplayMemory(env=env,
                               n_inputs=16,  # this is the state representation
                               n_outputs=env.action_space.n,
                               max_epsilon=1.0,
                               min_epsilon=0.05,
                               gamma=0.99,
                               tau=0.01,  # target network update rate
                               batch_size=64,
                               # last number if number of episodes - 0.04
                               lambda_=-np.log(0.01)/500,
                               replay_memory_capacity=1000,
                               learning_rate=0.0005,
                               prefill_memory=True,
                               random_actions=False,  # random agent
                               save_best_graph=True)

    start = time()
    agent.train(
        num_episodes=500,
    )
    end = time()
    print("time for training: ", str(end - start))
    print("mean is: ", np.mean(agent.rewards))
