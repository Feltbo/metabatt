#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jun 11 14:44:12 2021

@author: felix
"""
import numpy as np
import copy
from typing import List
import networkx as nx

from gym.envs.toy_text import discrete
from gym import spaces

from metabatt.graph import Graph, Edge, Node, Subgraphs
from metabatt.geometry import get_nearest_neighbors, wrap_point_in_pbc


class MetabattEnvSubgraphs(discrete.DiscreteEnv):
    """ 
    Parameters
    ------------
    action_space : obj
        Action space containing all valid subgraphs

    subgraphs : obj
        Subgraph object storing all graphs as nx.Graph() objects

    Notes
    ------------
    The goal of the environment is to extend a graph with accessible
    subgraphs until no more action is possible. The subgraphs contain
    the edge AND the nodes.
    Rules can be implemented by filtering transition state probabilites,
    i.e. defining the rules in the `legal_actions()` function
    """

    def __init__(self,
                 n_cells_x: float,
                 n_cells_y: float,
                 subgraphs,
                 pbc: List[bool] = [True, True],
                 allow_frustration: bool = False):
        self.n_cells_x = n_cells_x
        self.n_cells_y = n_cells_y
        self.subgraphs = subgraphs
        self.allow_frustration = allow_frustration
        self.pbc = pbc
        self.cell = np.array([[n_cells_x - 1, 0],
                              [0, n_cells_y - 1]])
        self.graph = self.get_initial_graph()
        self.edge_length = 0

        self.nA = len(subgraphs)

        self.action_space = spaces.Discrete(self.nA)

        # initialize state distribution
        isd = None  # also unknown

        P = {}  # unknown

        #probA = 1/nA
        #done = False
        # for s in range(nS):
        #    for a in range(nA):
        #        if s == nS - 1: # one before terminal state
        #            done = True
        #        # (nextstep, reward, done)
        #        reward = 0
        #        P[s][a] = (s + 1, reward, done)

        #super(MetabattEnvSubgraphs, self).__init__(nS, nA, P, isd)

    @classmethod
    def new_instance(cls, n_cells_x, n_cells_y, subgraphs, pbc):
        """ Assign new memory address """
        cls = cls(n_cells_x, n_cells_y, subgraphs, pbc)
        cls.s = 0
        cls.lastaction = None
        return cls

    @property
    def current_node(self):
        return self._current_node

    @current_node.setter
    def current_node(self, node):
        self._current_node = node

    def get_initial_graph(self):
        """ The initial graph needs to have at least on node """
        max_edge_length = max(self.subgraphs.lengths)
        graph = Graph(self.n_cells_x,
                      self.n_cells_y,
                      pbc=self.pbc,
                      cell=self.cell,
                      node_size=0.05,
                      eps_edge_overlaps=1e-3,
                      max_edge_length=max_edge_length,
                      nodes=[])
        self.graph = graph
        self.graph.add_node(node_id=0, position=[0, 0])
        current_node = self.graph.get_node(node_id=0)
        # TODO: now it is a hard coded initial rotation, could be both
        current_node.rotate(direction='L')
        self.current_node = current_node
        self.edge_density = 0

        # add the intial legal actions (all should be allowed)
        subgraphs = self.get_subgraphs_from_node(self.current_node)
        self.current_node.legal_actions = np.array(
            list(range(len(subgraphs.graphs))))
        return self.graph

    def step(self, a):
        self.lastaction = a

        cur_node = self.current_node
        # every node has different subraphs given different origins
        subgraphs = Subgraphs(
            angles=self.subgraphs.angles,
            lengths=self.subgraphs.lengths,
            node=cur_node)
        subgraph = [g for g in subgraphs.graphs if g.id == a][0]

        # execute step in environment
        added_edge_ids, added_nodes = self.graph.add_subgraph_to_node(
            cur_node, subgraph)
        # keep check below for now, but otherwise should not be necessary
        if len(added_nodes) == 0 and len(added_edge_ids) == 0:
            raise KeyError("No Nodes or edges or added")

        # assign possible legal actions on new nodes
        for node in added_nodes:
            node.legal_actions = self.filter_legal_actions(node)

        # select a new current "legal" node, node with available actions
        current_node = self.select_focal_node()

        d = False
        if current_node == 'done':
            d = True
        else:
            self.current_node = current_node

        # decide on reward
        area = self.graph.x_size * self.graph.y_size
        edge_density = self.graph.total_edge_length()/area
        e_d_increase = edge_density - self.edge_density
        r = e_d_increase
        self.edge_density += e_d_increase
        #r = 0
        #r = self.edge_density

        return r, d

    def select_focal_node(self):
        """ Choose current node that action is played on """
        while self.legal_nodes:
            node = np.random.choice(self.legal_nodes)
            # update legal actions here, reason: avoid doing it multiple times
            legal_actions = self.filter_legal_actions(node)
            if len(legal_actions) > 0:
                return node
            else:
                node.legal_actions = legal_actions
        # no legal nodes means episode is finished
        return 'done'

    @property
    def legal_nodes(self):
        return self.filter_legal_nodes()

    def filter_legal_nodes(self):
        """ Return all nodes that have legal actions available """
        legal_nodes = [node for node in self.graph.nodes
                       if len(node.legal_actions) > 0]  # at least one avail
        return legal_nodes

    def get_subgraphs_from_node(self, node):
        subgraphs = Subgraphs(angles=self.subgraphs.angles,
                              lengths=self.subgraphs.lengths,
                              node=node)
        return subgraphs

    def filter_legal_actions(self, node):
        """ Only return allowed subgraphs given the current node """
        legal_actions = []
        node_id_test = max(self.graph.node_ids) + 1
        subgraphs = self.get_subgraphs_from_node(node)
        for i, subgraph in enumerate(subgraphs.graphs):
            # TODO: this is redundant to adding the subgraph in the graph class
            # checking should be done at one place, otherwise have to implement
            # the logic twice, currently also in graph.add_subgraph_to_node()
            # extract the edge, for now only extracts single edges
            con_nodes_pos = list(
                nx.get_node_attributes(subgraph, 'pos').values())
            v = np.array(con_nodes_pos[1]) - np.array(con_nodes_pos[0])
            position = nx.get_node_attributes(subgraph, 'pos')[1]

            if self.pbc:
                position = wrap_point_in_pbc(
                    position[0], position[1], cell=self.graph.cell, pbc=self.graph.pbc)
            self.graph.add_node(
                node_id=node_id_test,
                position=position)
            node_candidate = self.graph.get_node(node_id=node_id_test)

            existing_node = self.graph.node_exists_already(node_candidate)
            if existing_node:
                node_candidate = existing_node
                self.graph.remove_node(node_id=node_id_test)

            # it is okay for the node to exist, it can still form a connection
            edge = Edge(node, node_candidate, self.graph, v=v)

            if self.graph.edge_exists_already(edge):
                self.clean_dummy_nodes(existing_node, node_id_test)
                continue

            # this avoids O(N^2) and makes O(N)
            edges = self.graph.edges_to_consider_for_overlap(
                self.graph.edges, edge)
            # start checking if straight line edge overlaps
            # Start and end points are the nodes
            exclude_ids = [node.node_id, node_candidate.node_id]
            nodes = [n for n in self.graph.nodes if n.node_id not in exclude_ids]
            if self.graph.straight_edge_overlaps(edge, edges, nodes):
                self.clean_dummy_nodes(existing_node, node_id_test)
                continue

            # does this connection frustrate the system
            #print(f"{self.allow_frustration=}")
            if (not self.allow_frustration) and self.edge_frustrates_system(edge, node, node_candidate):
                 self.clean_dummy_nodes(existing_node, node_id_test)
                 continue

            # checks that node is at a legal position
            eps_node = self.graph.node_size
            node_positions = np.array([node_candidate.position])
            # check if new node overlaps with straight edge
            if not existing_node:
                if self.graph.straight_edges_overlap_with_node(
                        edges, np.array(node_candidate.position), eps=eps_node):
                    self.clean_dummy_nodes(existing_node, node_id_test)
                    continue

            # check if new node overlaps with a bended edge
            if not existing_node:
                if self.graph.bended_edges_overlap_with_nodes(
                        edges=edges,
                        node_positions=node_positions,
                        eps=eps_node):
                    self.clean_dummy_nodes(existing_node, node_id_test)
                    continue

            # else it is only left to check if the bended edge overlaps
            self.graph.bend_edge(edge)
            if self.graph.new_bended_edge_overlaps(edge):
                self.clean_dummy_nodes(existing_node, node_id_test)
                continue

            # else we finally have found a valid action
            legal_actions.append(i)

            # clean up
            self.clean_dummy_nodes(existing_node, node_id_test)

        return np.array(legal_actions)

    def clean_dummy_nodes(self, existing_node, node_id):
        if not existing_node:
            self.graph.remove_node(node_id=node_id)

    def edge_frustrates_system(self, edge, node, ngb_node):
        """ Do the check and always keep the original rotations """
        # get original rotation list
        init_rotation = [n.rot_direction for n in self.graph.nodes]
        self.graph._edges[edge.edge_id].add(edge)
        node.add_neighbor(ngb_node.node_id)
        self.graph.nodes[ngb_node.node_id].add_neighbor(node.node_id)

        try:
            self.graph.assign_rotation_to_nodes()
        except AssertionError:
            self.graph.remove_edge(edge.edge_id)
            return True

        if edge.node1.rot_direction == edge.node2.rot_direction:
            self.graph.remove_edge(edge.edge_id)
            return True

        self.graph.remove_edge(edge.edge_id)
        # assign original rotations
        for i, node in enumerate(self.graph.nodes):
            node.rot_direction = init_rotation[i]
        return False

    def render(self):
        self.graph.plot()

    def reset(self):
        self.graph = None
        self.__init__(self.n_cells_x,
                      self.n_cells_y,
                      self.subgraphs,
                      self.pbc,
                      allow_frustration=self.allow_frustration)


class MetabattEnvSubgraphsDumbVersion(MetabattEnvSubgraphs):
    """ 
    Remove all legal action filtering making the environment 'dumb'.
    """

    def __init__(self, *args, **kwargs):
        super(MetabattEnvSubgraphsDumbVersion, self).__init__(*args, **kwargs)

    def step(self, a):
        self.lastaction = a

        cur_node = self.current_node
        # every node has different subraphs given different origins
        subgraphs = Subgraphs(
            angles=self.subgraphs.angles,
            lengths=self.subgraphs.lengths,
            node=cur_node)
        subgraph = [g for g in subgraphs.graphs if g.id == a][0]

        # execute step in environment
        d = False
        try:
            added_edge_ids, added_nodes = self.graph.add_subgraph_to_node(
                cur_node, subgraph)
        except:
            # Environment fails every time a non-legal action is chosen
            d = True
            area = self.graph.x_size * self.graph.y_size
            edge_density = self.graph.total_edge_length()/area
            r = self.edge_density
            return r, d

        # keep check below for now, but otherwise should not be necessary
        if len(added_nodes) == 0 and len(added_edge_ids) == 0:
            raise KeyError("No Nodes or edges or added")

        # select a new current node randomly
        self.current_node = self.select_focal_node(added_nodes)
        # decide on reward
        # not important for now, is played randomly
        area = self.graph.x_size * self.graph.y_size
        edge_density = self.graph.total_edge_length()/area
        e_d_increase = edge_density - self.edge_density
        r = e_d_increase
        self.edge_density += e_d_increase
        #r = 0
        r = self.edge_density

        return r, d

    def select_focal_node(self, added_nodes):
        """ Here comes the dumb part: We choose a node randomly unless
        the new added node is available
        """
        if len(added_nodes) > 0:
            return added_nodes[0]
        else:
            return np.random.choice(self.graph.nodes)
