"""
Collection of all different state representations
"""
import numpy as np
from shutil import copy

from metabatt.geometry import angle_between, wrap_point_in_pbc
from metabatt.shapes import LineShape


def one_hot(state, l):
    """One-hot encoder for the states"""
    a = np.zeros((len(state), l))
    a[range(len(state)), state] = 1
    return a


def graph_metadata(graph):
    """ Return useful info on graph state """
    metadata = []
    metadata.append(graph.n_nodes)
    metadata.append(len(graph.edges))
    metadata.append(graph.n_cells_h)
    metadata.append(graph.n_cells_w)
    return np.array([metadata])


def node_to_vector(graph, subgraphs, node):
    """ Transform node environment into a vector representation """
    # get all connected edges
    angles_edges = []
    connected_edges = graph.get_connected_edges_to_node(node)
    for edge in connected_edges:
        origin = node.position
        p2 = edge.node2.position
        v = edge.v
        if np.allclose(origin, p2):
            p2 = edge.node1.position
            v = v * (-1)
        p2 = origin + v  # explicitly not care about pbcs
        edge_shape = LineShape(origin, p2, v=v)
        angle_edge = np.rad2deg(edge_shape.get_rotation_angle_line())
        angles_edges.append(angle_edge)

    # mirrored at 180 degree, but need 360 for state representation
    subgraph_angles = [angle for angle in subgraphs.angles
                       if not np.isclose(angle, 0)
                       and not np.isclose(angle, 180)
                       and not angle > 180]
    angles_mirrored = np.array(subgraph_angles) + 180
    subgraph_angles = [angle for angle in subgraphs.angles
                       if not angle > 180]
    subgraph_angles = subgraph_angles + list(angles_mirrored)
    node_vec = np.zeros(len(subgraph_angles))
    for i, angle in enumerate(subgraph_angles):
        if np.any([np.isclose(angle, angle_edge) for angle_edge in angles_edges]):
            node_vec[i] = 1
    return np.array([node_vec])


def connected_nodes_to_vector(graph, subgraphs, node):
    # node vector
    nodes_vector = node_to_vector(graph, subgraphs, node)[0]
    connected_edges = graph.get_connected_edges_to_node(node)

    # mirrored at 180 degree, but need 360 for state representation
    subgraph_angles = [angle for angle in subgraphs.angles
                       if not np.isclose(angle, 0)
                       and not np.isclose(angle, 180)
                       and not angle > 180]
    angles_mirrored = np.array(subgraph_angles) + 180
    subgraph_angles = [angle for angle in subgraphs.angles
                       if not angle > 180]
    subgraph_angles = subgraph_angles + list(angles_mirrored)

    for i, angle in enumerate(subgraph_angles):
        angle_connected = False
        for edge in connected_edges:
            origin = node.position
            p2 = edge.node2.position
            v = edge.v
            if np.allclose(origin, p2):
                p2 = edge.node1.position
                v = v * (-1)
            p2 = origin + v  # explicitly not care about pbcs
            p2 = wrap_point_in_pbc(
                p2[0], p2[1], cell=graph.cell, pbc=graph.pbc)
            ngb_node = [node for node in graph.nodes
                        if np.isclose(node.position, p2).all()][0]
            edge_shape = LineShape(origin, p2, v=v)
            angle_edge = np.rad2deg(edge_shape.get_rotation_angle_line())
            if np.isclose(angle, angle_edge):
                angle_connected = True
                ngb_node_vec = node_to_vector(graph, subgraphs, ngb_node)[0]
                nodes_vector = np.hstack((nodes_vector, ngb_node_vec))
                break

        if not angle_connected:
            n_angles = len(subgraph_angles)
            nodes_vector = np.hstack(
                (nodes_vector, np.zeros(n_angles)))
    return nodes_vector
