"""
Stores all accessible agents
"""
import numpy as np
import torch
import abc
from collections import defaultdict
import json

import matplotlib.pyplot as plt

from metabatt.graph import Subgraphs
from metabatt.algorithms import moving_average
from metabatt.reinforce.models import QNetwork, DQN, ReplayMemory
from metabatt.reinforce.environments import MetabattEnvSubgraphs
from metabatt.reinforce.state_representation import one_hot,\
    graph_metadata,\
    node_to_vector,\
    connected_nodes_to_vector


class Agent:
    """ Base class for all agents """

    @abc.abstractmethod
    def create_model():
        """ Create the main model of the agent """
        pass

    @abc.abstractmethod
    def train():
        """ Train the agent for given number of episodes """
        pass

    def save_hyperparameters(self):
        """" Store all relevant arguments to reproduce trained model """

    def save_model(self, model, file_name):
        """" Save the weights of the trained model """
        torch.save(model.state_dict(), f'{file_name}.pth')

    def load_model(self, file_name):
        model = self.create_model(
            self.n_inputs, self.n_outputs, self.learning_rate)
        model.load_state_dict(torch.load(f'{file_name}.pth'))
        model.eval()
        return model

    def plot_results(self, num_episodes, rewards, lengths, losses, epsilons):
        plt.figure(figsize=(16, 9))
        plt.subplot(411)
        plt.title('training rewards')
        plt.plot(range(1, num_episodes+1), rewards)
        plt.plot(range(1, num_episodes+1), moving_average(rewards))
        plt.xlim([0, num_episodes])
        plt.subplot(412)
        plt.title('training lengths')
        plt.plot(range(1, num_episodes+1), lengths)
        plt.plot(range(1, num_episodes+1), moving_average(lengths))
        plt.xlim([0, num_episodes])
        plt.subplot(413)
        plt.title('training loss')
        plt.plot(range(1, num_episodes+1), losses)
        plt.plot(range(1, num_episodes+1), moving_average(losses))
        plt.xlim([0, num_episodes])
        plt.subplot(414)
        plt.title('epsilon')
        plt.plot(range(1, num_episodes+1), epsilons)
        plt.xlim([0, num_episodes])
        plt.tight_layout()
        plt.show()
        plt.figure()
        max_list = []
        range_list = []
        cur_max_value = 0
        for i, value in enumerate(rewards):
            if cur_max_value < value:
                # get a step in the plot
                if i != 0:
                    max_list.append(cur_max_value)
                    range_list.append(i + 1)
                cur_max_value = value
                max_list.append(cur_max_value)
                range_list.append(i + 1)
        range_list.append(num_episodes)
        max_list.append(cur_max_value)
        plt.plot(range_list, max_list)
        plt.ylim([min(max_list) - 1, max(max_list) + 1])
        plt.xlim([0, num_episodes])
        plt.xlabel('Number of episodes')
        plt.ylabel('Maximum reward')
        print(max_list)
        print(range_list)


class QAgentReplayMemory(Agent):
    """ Add subgraphs on a canvas using replay memory for off-policy learning """

    def __init__(self,
                 env,
                 n_inputs: int,
                 n_outputs: int,
                 learning_rate: float,
                 max_epsilon: float = 1.0,
                 min_epsilon: float = 0.1,
                 gamma: float = 0.9,
                 val_freq: float = 10,
                 tau: float = 0.01,  # target network update rate
                 lambda_: float = 0.01,  # speed of decay for epsilon value
                 batch_size: int = 64,
                 replay_memory_capacity: int = 10000,
                 prefill_memory: bool = True,
                 random_actions: bool = False,
                 save_best_graph: bool = False,
                 save_best_graph_filename: str = "best_graph"):
        self.env = env
        self.n_inputs = n_inputs
        self.n_outputs = n_outputs
        self.learning_rate = learning_rate
        self.epsilon = max_epsilon
        self.max_epsilon = max_epsilon
        self.min_epsilon = min_epsilon
        self.gamma = gamma
        self.val_freq = val_freq
        self.tau = tau
        self.lambda_ = lambda_
        self.batch_size = batch_size
        self.replay_memory_capacity = replay_memory_capacity
        self.prefill_memory = prefill_memory
        self.random_actions = random_actions
        self.save_best_graph = save_best_graph
        self.save_best_graph_filename = save_best_graph_filename

        # initialize DQN and replay memory
        self.policy_dqn = self.create_model(
            self.n_inputs, self.n_outputs, self.learning_rate)
        self.target_dqn = self.create_model(
            self.n_inputs, self.n_outputs, self.learning_rate)
        self.target_dqn.load_state_dict(self.policy_dqn.state_dict())

        self.replay_memory = ReplayMemory(self.replay_memory_capacity)

        # bookkeeping
        self.rewards = []
        self.lengths = []
        self.losses = []
        self.epsilons = []
        self.Qs = []

    @classmethod
    def from_hyperparameters(cls, fname='hyperparams'):
        """ Intialize given a set of hyperaparemeters """
        with open(f'{fname}.json', 'r') as f:
            hyper_dic = json.load(f)
        subgraphs = Subgraphs(**hyper_dic['subgraphs'])
        env = MetabattEnvSubgraphs(subgraphs=subgraphs,
                                   **hyper_dic['env'])
        cls = cls(env=env,
                  **hyper_dic['agent'])
        cls.hyper_dic = hyper_dic
        return cls

    def create_model(self, n_inputs, n_outputs, learning_rate):
        model = DQN(n_inputs, n_outputs, learning_rate)
        return model

    def save_hyperparameters(self, fname='hyperparams'):
        """ This could be done more elegant using some of pytorch modules """
        hyper_dic = defaultdict(dict)
        agent_params = ['n_inputs',
                        'n_outputs',
                        'learning_rate',
                        'max_epsilon',
                        'min_epsilon',
                        'gamma',
                        'val_freq',
                        'tau',
                        'lambda_',
                        'batch_size',
                        'replay_memory_capacity',
                        'prefill_memory',
                        'random_actions',
                        'save_best_graph'
                        ]
        train_params = ['num_episodes']
        env_params = ['n_cells_x', 'n_cells_y']
        subgraph_params = ['angles', 'lengths']

        for param in agent_params:
            hyper_dic['agent'][param] = vars(self)[param]
        for param in env_params:
            hyper_dic['env'][param] = vars(self.env)[param]
        for param in subgraph_params:
            hyper_dic['subgraphs'][param] = vars(self.env.subgraphs)[param]
        for param in train_params:
            # check if there, otherwise agent hastn been trained
            if hasattr(self, param):
                hyper_dic['train'][param] = vars(self)[param]

        # store values
        with open(f'{fname}.json', 'w') as f:
            json.dump(hyper_dic, f, indent=4)

    def prefill_replay_memory(self):
        print('prefill replay memory')
        s = self.env.reset()
        while self.replay_memory.count() < self.replay_memory_capacity:
            mask = self.get_legal_actions()
            action_inds = np.array(range(self.env.nA))[mask]
            self.env.action_space.n = len(action_inds)
            a = action_inds[self.env.action_space.sample()]
            s = self.get_graph_state()
            r, d = self.env.step(a)
            if not d:
                s1 = self.get_graph_state()
                self.replay_memory.add(s, a, r, s1, d)
            s = self.get_graph_state() if not d else self.env.reset()

    def train(self, num_episodes, max_reward=0):
        """ Train the agent for a number of episodes """
        self.num_episodes = num_episodes  # bookkeeping
        # prefill replay memory with random actions
        if self.prefill_memory:
            self.prefill_replay_memory()

        print('start training')

        for i in range(num_episodes):
            s, ep_reward, ep_loss = self.env.reset(), 0, 0
            ep_reward, ep_loss, train_length = self.train_episode(s)

            if ep_reward > max_reward:
                print(" new max record arrange grid: ", ep_reward)
                max_reward = ep_reward
                self.env.render()
                if self.save_best_graph:
                    self.env.graph.save_graph_state(self.save_best_graph_filename)

            # self.epsilon *= num_episodes/(i/(num_episodes/20)+num_episodes) # decrease epsilon
            min_eps = self.min_epsilon
            max_eps = self.max_epsilon
            self.epsilon = min_eps + \
                (max_eps - min_eps) * np.exp(-self.lambda_ * i)
            # bookkeping
            self.epsilons.append(self.epsilon)
            self.rewards.append(ep_reward)
            self.lengths.append(train_length+1)
            self.losses.append(ep_loss)
            if (i+1) % self.val_freq == 0:
                print('{:5d} mean training reward: {:5.2f}'.format(
                    i+1, np.mean(self.rewards[-self.val_freq:])))

        print('done')
        self.plot_results(
            num_episodes, self.rewards, self.lengths, self.losses, self.epsilons)

    def select_action(self):
        # Selection with epsilon-greedy
        # first filter out illegal actions
        mask = self.get_legal_actions()
        action_inds = np.array(range(self.env.nA))[mask]
        self.env.action_space.n = len(action_inds)
        # select action with epsilon-greedy strategy
        if np.random.rand() < self.epsilon or self.random_actions:
            a = action_inds[self.env.action_space.sample()]
        else:
            with torch.no_grad():
                Q_policy = self.policy_dqn(
                    torch.from_numpy(self.get_graph_state()).float())
                a = action_inds[Q_policy.detach().numpy()[0][mask].argmax()]
        return a

    def execute_action(self, a):
        s = self.get_graph_state()
        r, done = self.env.step(a)
        s1 = self.get_graph_state()
        return s, r, s1, done

    def train_episode(self, s, episode_limit=1000, ep_reward=0, ep_loss=0):
        """ Train a single episode """
        for train_length in range(episode_limit):
            # choose an action
            a = self.select_action()

            # perform action
            s, r, s1, done = self.execute_action(a)

            # store experience in replay memory
            self.replay_memory.add(s, a, r, s1, done)

            # batch update
            if self.replay_memory.count() >= self.batch_size:
                # sample batch from replay memory
                batch = np.array(self.replay_memory.sample(
                    self.batch_size), dtype=object)
                ss, aa, rr, ss1, dd = batch[:, 0], batch[:,
                                                         1], batch[:, 2], batch[:, 3], batch[:, 4]
                ss = np.array(np.vstack(ss))
                ss1 = np.array(np.vstack(ss1))
                # do forward pass of batch
                self.policy_dqn.optimizer.zero_grad()
                Q_policy = self.policy_dqn(torch.from_numpy(ss).float())
                # use target network to compute target Q-values
                with torch.no_grad():
                    # TODO: use target net
                    Q_target = self.target_dqn(torch.from_numpy(ss1).float())
                # compute target for each sampled experience
                q_targets = Q_policy.clone()
                for k in range(self.batch_size):
                    #q_targets[k, aa[k]] = rr[k] + self.gamma * Q_target[k].max().item() * (not dd[k])
                    # Double DQN
                    q_targets[k, aa[k]] = rr[k] + self.gamma * \
                        Q_target[k][np.argmax(
                            Q_policy.detach().numpy()[k])] * (not dd[k])
                # update network weights
                loss = self.policy_dqn.loss(Q_policy, q_targets)
                loss.backward()
                self.policy_dqn.optimizer.step()
                # update target network parameters from policy network parameters
                self.target_dqn.update_params(
                    self.policy_dqn.state_dict(), self.tau)
            else:
                loss = 0

            # 6. bookkeeping
            ep_reward += r
            ep_loss += loss.item()

            if train_length == episode_limit:
                print(f"EPISODE LIMIT {episode_limit} REACHED")
            if done:
                area = self.env.graph.x_size * self.env.graph.y_size
                ep_reward = self.env.graph.total_edge_length()/area
                break

        return ep_reward, ep_loss, train_length

    def predict(self, model, num_episodes, epsilon=0, save_best_graph=False):
        """ Use the current policy DQN to run the envrionment """
        print(' start prediction using pretrained model '.center(50, "-"))
        self.epsilon = epsilon
        self.policy_dqn = model

        max_reward = 0
        for i in range(num_episodes):
            s, ep_reward = self.env.reset(), 0
            ep_reward = self.predict_episode()

            if ep_reward > max_reward:
                print(" new max record arrange grid: ", ep_reward)
                max_reward = ep_reward
                self.env.render()
                if save_best_graph:
                    self.env.graph.save_graph_state('best_graph')

            # bookkeping
            self.epsilons.append(self.epsilon)
            self.rewards.append(ep_reward)

            if (i+1) % self.val_freq == 0:
                print('{:5d} mean prediction reward: {:5.2f}'.format(
                    i+1, np.mean(self.rewards[-self.val_freq:])))

        print(' done '.center(50, "-"))

    def predict_episode(self, episode_limit=1000, ep_reward=0):
        for train_length in range(episode_limit):
            a = self.select_action()

            # perform action
            s, r, s1, done = self.execute_action(a)

            # bookkeeping
            ep_reward += r

            if train_length == episode_limit:
                print(f"EPISODE LIMIT {episode_limit} REACHED")
            if done:
                area = self.env.graph.x_size * self.env.graph.y_size
                ep_reward = self.env.graph.total_edge_length()/area
                break
        return ep_reward

    def get_graph_state(self):
        """ State representation """
        metadata = graph_metadata(self.env.graph)
        node_vec = node_to_vector(
            self.env.graph, self.env.subgraphs, self.env.current_node)
        graph_vec = np.hstack((metadata, node_vec))

        # graph_vec = connected_nodes_to_vector(
        #     self.env.graph, self.env.subgraphs, self.env.current_node)
        # graph_vec = np.array([graph_vec])
        return graph_vec

    def get_legal_actions(self):
        """ Return only actions that are allowed """
        legal_actions = self.env.filter_legal_actions(self.env.current_node)
        mask = [ind in list(legal_actions) for ind in range(self.env.nA)]
        return mask
