"""
Any basic (and not so basic) geometry functions
"""
import copy
import numpy as np

from ase.geometry import get_distances, wrap_positions

from src.cython.geometry import lines_intersect_cpp


class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __eq__(self, p2):
        return np.isclose(self.x, p2.x) and np.isclose(self.y, p2.y)


def on_segment(p, q, r):
    """ Check if point q lies on segment 'pr'

    Notes
    ------
    Edges are allowed to touch same node point
    """
    if ((q.x < max(p.x, r.x)) and (q.x > min(p.x, r.x)) and
            (q.y < max(p.y, r.y)) and (q.y > min(p.y, r.y))):
        return True
    return False


def orientation(p, q, r):
    """ Find orientation of ordered triplet 

    Notes
    ------
    0 : Colinear
    1 : Clockwise
    2 : Counterclockwise

    """
    val = (float(q.y - p.y) * (r.x - q.x)) - (float(q.x - p.x) * (r.y - q.y))

    if val > 0:
        return 1
    elif val < 0:
        return 2
    else:
        return 0


def do_intersect(p1, q1, p2, q2):
    """ Line segments 'p1q1' and 'p2q2' do intersect 

    Notes
    ------
    all credit to: https://www.geeksforgeeks.org/check-if-two-given-line-segments-intersect/
    """
    #
    p1 = Point(p1[0], p1[1])
    q1 = Point(q1[0], q1[1])
    p2 = Point(p2[0], p2[1])
    q2 = Point(q2[0], q2[1])

    # four possible orientations
    o1 = orientation(p1, q1, p2)
    o2 = orientation(p1, q1, q2)
    o3 = orientation(p2, q2, p1)
    o4 = orientation(p2, q2, q1)

    # different orientations mean intersection
    if ((o1 != o2) and (o3 != o4)):
        # edges touching is okay
        if p1 == p2 or p1 == q2 or q1 == p2 or q1 == q2:
            pass
        else:
            return True

    # p1 , q1 and p2 are colinear and p2 lies on segment p1q1
    if ((o1 == 0) and on_segment(p1, p2, q1)):
        return True

    # p1 , q1 and q2 are colinear and q2 lies on segment p1q1
    if ((o2 == 0) and on_segment(p1, q2, q1)):
        return True

    # p2 , q2 and p1 are colinear and p1 lies on segment p2q2
    if ((o3 == 0) and on_segment(p2, p1, q2)):
        return True

    # p2 , q2 and q1 are colinear and q1 lies on segment p2q2
    if ((o4 == 0) and on_segment(p2, q1, q2)):
        return True

    # If none of the cases
    return False


def lines_intersect(line1, line2, eps=1e-2):
    return lines_intersect_cpp(
        line1[:, 0], line1[:, 1], line2[:, 0], line2[:, 1], eps=eps)


def lines_intersect_numpy(line1, line2, eps=1e-2):
    """ Two lines with line=[[x1, y1], [x2, y2], ...] intersect """
    line1 = np.array(line1)
    line2 = np.array(line2)

    for pos in line1:
        if np.any(np.linalg.norm(line2 - pos, axis=1) < eps):
            return True
    return False


def lines_intersect_python(line1, line2, eps=1e-2):
    """ Two lines with line=[[x1, y1], [x2, y2], ...] intersect """
    line1 = list(line1)
    line2 = list(line2)

    if any([((x1-x2)**2 + (y1-y2)**2)**(1/2) < eps
            for x2, y2 in line2 for x1, y1 in line1]):
        return True
    return False


def rotate_point(origin, point, angle):
    """
    Rotate a point counterclockwise by a given angle around a given origin.

    The angle should be given in radians.
    """
    ox, oy = origin
    px, py = point

    qx = ox + np.cos(angle) * (px - ox) - np.sin(angle) * (py - oy)
    qy = oy + np.sin(angle) * (px - ox) + np.cos(angle) * (py - oy)
    return qx, qy


def rotate_line(origin, line, angle):
    """
    Rotate a point counterclockwise by a given angle around a given origin.

    The angle should be given in radians.
    """
    R = np.array([[np.cos(angle), -np.sin(angle)],
                  [np.sin(angle), np.cos(angle)]])
    line_rot = R@line.T
    line_rot = line_rot.T + origin
    return line_rot


def angle_between(v1, v2):
    """ Returns the angle in radians between vectors 'v1' and 'v2'::

    Notes
    -------
    This handles cases when vectors have same/opposite directions
    """
    v1_u = normalize(v1)
    v2_u = normalize(v2)
    return np.arccos(np.clip(np.dot(v1_u, v2_u), -1.0, 1.0))


def normalize(v):
    norm = np.linalg.norm(v)
    if norm == 0:
        return v
    return v / norm


def perpendicular(v):
    """ Find perpendicular vector b to v """
    b = np.empty_like(v)
    b[0] = -v[1]
    b[1] = v[0]
    return b


def find_intersection_line(p1, p2, q1, q2):
    """ Find intersecting point given two arrays """
    p1 = np.array(p1)
    q1 = np.array(q1)
    p2 = np.array(p2)
    q2 = np.array(q2)
    d_p = p2 - p1
    d_q = q2 - q1
    pq = p1 - q1
    dap = perpendicular(d_p)
    denom = np.dot(dap, d_q)
    num = np.dot(dap, pq)
    return (num / denom.astype(float))*d_q + q1


def get_nearest_neighbors(point, points, cut_off=1.5, cell=None, pbc=None):
    """ ASE function is used, since it can handle pbcs """
    points = np.hstack((points, np.zeros((len(points), 1))))
    point = np.append(point, 0)

    if cell is not None:
        cell = cell_2d_to_3d(cell)
        pbc_temp = copy.deepcopy(pbc)
        pbc_temp += [False]
    else:
        pbc_temp = pbc

    _, dists = get_distances(points, point, cell=cell, pbc=pbc_temp)
    ngb_inds = np.argwhere(dists.flatten() < cut_off)
    return ngb_inds.flatten()


def cell_2d_to_3d(cell_2d):
    return np.pad(cell_2d, (0, 1))


def p_to_p_distance(p1, p2, cell=None, pbc=None, return_vec=False):
    """ Return distance between two points """
    p1 = np.append(p1, 0)
    p2 = np.append(p2, 0)

    if cell is not None:
        cell = cell_2d_to_3d(cell)
        pbc_temp = copy.deepcopy(pbc)
        pbc_temp += [False]
    else:
        pbc_temp = pbc

    vec, dists = get_distances(p1, p2, cell=cell, pbc=pbc_temp)
    if return_vec:
        return vec.flatten(), dists.flatten()
    return dists.flatten()


def p_translated_pbcs(p1, p2, graph, trans_v=False):
    """ Translate point if vector p1->p2 crosses pbcs """
    p1_temp = np.array(p1)
    p1 = np.array(p1)
    p2 = np.array(p2)
    v, dists = p_to_p_distance(
        p1, p2, graph.cell, graph.pbc, return_vec=True)
    if p_outside_pbcs(p1 + v[:2], graph.x_size, graph.y_size):
        x, y = p1
        x = x - np.floor(v[0]/graph.x_size) * graph.x_size
        y = y - np.floor(v[1]/graph.y_size) * graph.y_size
        p1 = [x, y]
    if trans_v:
        return p1, p1 - p1_temp
    return p1


def wrap_point_in_pbc_handmade(x, y, x_size, y_size):
    x = x - np.floor(x / x_size) * x_size
    y = y - np.floor(y / y_size) * y_size
    return x, y


def p_outside_pbcs(point, x_size, y_size):
    """ Does point cross the pbcs in any direciton """
    x, y = point
    if np.floor(x/x_size) and not np.isclose(x, x_size):
        return True
    if np.floor(y/y_size) and not np.isclose(y, y_size):
        return True
    return False


def wrap_point_in_pbc(x, y, cell=None, pbc=None):
    if cell is not None:
        pbc_temp = copy.deepcopy(pbc)
        cell = cell_2d_to_3d(cell)
        pbc_temp += [False]
    else:
        pbc_temp = pbc
    points = np.array([[x, y, 0]])
    points = wrap_positions(points, cell, pbc=pbc_temp)
    return points[0, :2]


def wrap_points_in_pbc(points, cell=None, pbc=None):
    points = np.c_[np.array(points), [0]*len(points)]
    if cell is not None:
        pbc_temp = copy.deepcopy(pbc)
        cell = cell_2d_to_3d(cell)
        pbc_temp += [False]
    else:
        pbc_temp = pbc
    points = wrap_positions(points, cell, pbc=pbc_temp)
    return points[:, :2]


def dist_p_to_straight_line(p, l1, l2, cell=None, pbc=None):
    """ Measures the distance between p and line segment between l1 to l2 """
    p = np.array(p)
    l1 = np.array(l1)
    l2 = np.array(l2)

    if pbc is not None:
        # TODO: cheap check to see if points across pbcs
        # Problem: dont know what distance p to l1 l2 should be, but I know
        # the length of the line
        # save way to move onto same point and then add pbc vector
        v, dists = p_to_p_distance(l1, l2, cell, pbc, return_vec=True)
        l2 = l1 + v[:2]
        # do the same for point to check, worst case translate back and forth
        v, dists = p_to_p_distance(l1, p, cell, pbc, return_vec=True)
        p = l1 + v[:2]

    # credit to https://gist.github.com/nim65s/5e9902cd67f094ce65b0
    # checking distance and for special cases
    if all(l1 == p) or all(l2 == p):
        return 0
    if all(l1 == l2):
        return np.linalg.norm(l1, p)
    if np.arccos(np.clip(np.dot((p - l1) / np.linalg.norm(p - l1), (l2 - l1) / np.linalg.norm(l2 - l1)), -1, 1)) > np.pi / 2:
        return np.linalg.norm(p - l1)
    if np.arccos(np.clip(np.dot((p - l2) / np.linalg.norm(p - l2), (l1 - l2) / np.linalg.norm(l1 - l2)), -1, 1)) > np.pi / 2:
        return np.linalg.norm(p - l2)
    return np.linalg.norm(np.cross(l1 - l2, l1 - p))/np.linalg.norm(l2-l1)

    return
