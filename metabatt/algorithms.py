"""
Algorithms used on graph
"""
import numpy as np


def assign_rotation_BFS(source_node, graph, init_rotation='R'):
    """ Implements the Breadth First Search for Graphs """
    connected_nodes = graph.get_connected_nodes()
    nodes = graph.nodes
    visited = np.zeros(len(nodes), dtype=bool)
    visited_nodes = []
    queue = []

    # mark source node
    queue.append(source_node)
    visited_nodes.append(source_node.node_id)
    visited[source_node.node_id] = True
    # check for connected neighbors
    ngb_nodes = [
        n for n in connected_nodes if n.node_id in source_node.neighbors]
    if any([node.rotated for node in ngb_nodes]):
        valid_rotation = source_node.get_valid_rotation_direction(
            connected_nodes)
        source_node.rotate(valid_rotation)
    else:
        source_node.rotate(init_rotation)

    while queue:
        # get node
        source_node = queue.pop(0)
        # go through all adjacent vertices
        for ngb_node in source_node.neighbors:
            if visited[ngb_node] == False:
                queue.append(nodes[ngb_node])
                visited[ngb_node] = True
                visited_nodes.append(ngb_node)
                if nodes[ngb_node].rotated == False:
                    nodes[ngb_node].rotate_valid_direction(nodes)


def moving_average(a, n=10):
    ret = np.cumsum(a, dtype=float)
    ret[n:] = ret[n:] - ret[:-n]
    return ret / n
