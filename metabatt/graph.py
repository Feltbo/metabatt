"""
First test script to build a graph
"""
# %%
from ast import Not
import os
import datetime
import math
import numpy as np
from collections import defaultdict
import itertools
import copy
import pickle
from typing import List, Sequence

import matplotlib.pyplot as plt
from matplotlib.ticker import (MultipleLocator, FormatStrFormatter,
                               AutoMinorLocator)

import networkx as nx

from metabatt.geometry import do_intersect,\
    lines_intersect,\
    get_nearest_neighbors,\
    p_to_p_distance,\
    wrap_point_in_pbc,\
    p_translated_pbcs,\
    p_outside_pbcs,\
    dist_p_to_straight_line,\
    find_intersection_line,\
    wrap_points_in_pbc
from metabatt.algorithms import assign_rotation_BFS
from metabatt.shapes import SineWaveHalf, SineWave
from metabatt.utils import class_from_module


class Graph:
    """ Basic graph class """

    def __init__(self,
                 n_cells_w: float,
                 n_cells_h: float,
                 max_edge_length: float,
                 x_intercalated: float = 1.0,  # level of ions intercalated
                 node_shape: str = 'Circle',
                 node_size: float = 0.1,
                 eps_edge_overlaps: float = 1e-2,  # overlapping bended edges
                 nodes=[],
                 pbc: List[bool] = None,
                 cell: np.ndarray = None):
        """

        Parameters
        ----------
        n_cells_w : float
            Width of cell
        n_cells_h : float
            Height of cell
        max_edge_length : float
            Maximum edge length in graph - will be 1.5x to check for overlap
        x_intercalated : float, optional
            TODO: For now not considered in the calcultions. This should then 
            decidethe shape in future versions. The default is 1.0.
        node_shape : str, optional
            Currently only Circle is implemented but other node shapes could
            be possible. The default is 'Circle'.
        node_size : float, optional
            Size of the node to check for overlaps between edges and nodes.
            The default is 0.1.
        eps_edge_overlaps : float, optional
            This is an important value! It decides if a bended edge overlaps or 
            not and therefore the amount of discretization values for the 
            bended edges. If overlapping bended edges are observed, try to 
            lower this value. Choose wisely - computational time increases!
            The default is 1e-2.
        nodes: List, optional
            A list of node objects can be provided from which the graph will 
            be initialized from. The default is [].
        pbc : List[bool], optional
            Currently only PBCs in both directions are tested [True, True].
            The non-periodic version is not tested. The default is None.
        cell : np.ndarray, optional
            Will be created automatically if width x height is known.
            The default is None.
        """
        self._edges = defaultdict(set)
        self.n_cells_h = n_cells_h
        self.n_cells_w = n_cells_w
        self.max_edge_length = max_edge_length
        self.x_intercalated = x_intercalated
        self.node_shape = node_shape
        self.node_size = node_size
        self.eps_edge_overlaps = eps_edge_overlaps
        self.nodes = nodes
        self.pbc = pbc
        if self.pbc is False:
            self.pbc = None
        # now create unit cell vectors if not present
        self.cell = cell
        if len(pbc) > 0 or pbc is True:
            if self.cell is None:
                self.cell = np.array([[n_cells_w - 1, 0],
                                      [0, n_cells_h - 1]])
            self.x_size = np.linalg.norm(self.cell[0])
            self.y_size = np.linalg.norm(self.cell[1])

    @property
    def n_nodes(self):
        return len(self.nodes)

    @property
    def node_ids(self):
        return [node.node_id for node in self.nodes]

    def total_edge_length(self):
        return sum([edge.length for edge in self.edges])

    def add_node(self, node_id: int, position):
        new_node = Node(
            node_id=node_id,
            position=np.array(position))
        self.nodes.append(new_node)
        self.assign_shape_to_node(new_node)

    def get_node(self, node_id: int):
        return [node for node in self.nodes if node.node_id == node_id][0]

    def get_edge(self, edge_id: str):
        """ Retrieve directly from defaultdict """
        return next(iter(self._edges[edge_id]))

    def remove_node(self, node_id: int):
        node = self.get_node(node_id)
        ngb_nodes = copy.deepcopy(node.neighbors)
        for ngb_node in ngb_nodes:
            edge = Edge(node, self.nodes[ngb_node], self)
            self.remove_edge(edge.edge_id)
        # rename nodeids
        rename_nodes = [node for node in self.nodes if node.node_id > node_id]
        for rename_node in rename_nodes:
            rename_node.node_id -= 1
        self.nodes.remove(node)

    def reset(self):
        self.__init__(self.n_cells_w,
                      self.n_cells_h,
                      self.max_edge_length,
                      self.x_intercalated,
                      self.node_shape,
                      self.node_size,
                      self.eps_edge_overlaps,
                      self.pbc,
                      self.cell
                      )

    def save_graph_state(self, file_name: str) -> None:
        state_dic = defaultdict()
        state_dic['vars'] = vars(self)
        fname = f"{file_name}.pickle"
        if os.path.isfile(fname):
            os.remove(fname)
        with open(fname, "wb") as output_file:
            pickle.dump(state_dic, output_file)

    @classmethod
    def load_graph_state(cls, file_name: str) -> None:
        with open(f"{file_name}.pickle", "rb") as input_file:
            state_dic = pickle.load(input_file)
        vars_dic = state_dic['vars']
        cls = cls(vars_dic['n_cells_w'],
                  vars_dic['n_cells_h'],
                  vars_dic['max_edge_length'],
                  vars_dic['node_size'],
                  vars_dic['eps_edge_overlaps'],
                  pbc=vars_dic['pbc'],
                  cell=vars_dic['cell'])
        cls.nodes = state_dic['vars']['nodes']
        cls._edges = state_dic['vars']['_edges']
        cls.x_size = vars_dic['x_size']
        cls.y_size = vars_dic['y_size']
        # Calculate the length for the edges
        new_edges = defaultdict(set)
        for edge_id, edge in cls._edges.items():
            edge = edge.pop()
            new_edges[edge_id] = {Edge(edge.node1, edge.node2, cls, edge.v)}
        cls._edges = new_edges

        return cls

    def reset_rotations(self) -> None:
        for node in self.nodes:
            node.rot_direction = None
            node.rotated = False

    def get_connected_nodes(self):
        edge_ids = np.array(
            [key.split('-') for key in self._edges.keys()],
            dtype=int).ravel()
        return [n for n in self.nodes if n.node_id in np.unique(edge_ids)]

    def get_connected_edges_to_node(self, node):
        """ Return all edges connected to a node - based on edge ids """
        connected_edges = []
        for edge_id in self._edges.keys():
            orig_id = edge_id
            if len(edge_id.split('_')) > 1:
                edge_id = edge_id.split('_')[0]
            node_id_1, node_id_2 = edge_id.split('-')
            if int(node.node_id) in [int(node_id_1), int(node_id_2)]:
                connected_edges.append(self.get_edge(orig_id))
        return connected_edges

    @property
    def edges(self):
        return [item for sublist in self._edges.values() for item in sublist]

    def add_edges_to_nodes(self):
        for node in self.nodes:
            _ = self.add_edges_to_node(node, node.state)

    def add_subgraph_to_node(self, node, subgraph):
        # succesful if at least one edge is drawn
        added_edge_ids = []
        added_nodes = []
        con_nodes_pos = list(nx.get_node_attributes(subgraph, 'pos').values())
        v = np.array(con_nodes_pos[1]) - np.array(con_nodes_pos[0])
        # filter out overlapping nodes
        for con_node_pos in con_nodes_pos:
            if not con_node_pos:
                break
            try:
                x, y = con_node_pos
                if self.pbc:
                    # translate back into cell
                    x, y = wrap_point_in_pbc(x, y, self.cell, self.pbc)
                    con_node_pos = [x, y]
                if x < 0 and not np.isclose(x, 0) or y < 0 and not np.isclose(y, 0):
                    raise IndexError(f"x/y smaller 0 {x}/{y}")
                is_focal_node = [n for n in self.nodes
                                 if np.isclose(node.position, con_node_pos).all()]
                # case it is the same node
                if len(is_focal_node) > 0:
                    con_node = is_focal_node[0]
                    continue
                # else, this is a new node or an existing one in the graph
                node_id = max(self.node_ids) + 1  # newest node
                self.add_node(node_id, position=con_node_pos)
                con_node = self.get_node(node_id)
                if self.node_exists_already(con_node, return_node=False):
                    self.remove_node(node_id=node_id)
                    con_node = self.node_exists_already(con_node)
                edge = Edge(node, con_node, self, v=v)
                if self.edge_exists_already(edge):
                    raise NameError("This should not happen - edge exists")
                    continue
                exclude_ids = [node.node_id, con_node.node_id]
                nodes = [n for n in self.nodes if n.node_id not in exclude_ids]
                if self.straight_edge_overlaps(edge, self.edges, nodes):
                    for edge_id in added_edge_ids:
                        self.remove_edge(edge_id)
                    return []

                self.bend_edge(edge)
                if self.new_bended_edge_overlaps(edge):
                    for edge_id in added_edge_ids:
                        self.remove_edge(edge_id)
                    return []

                # add rotation direction by making known to graph class
                self.add_edge(edge)
                self.assign_rotation_to_nodes()

                # bookkeeping
                added_edge_ids.append(edge.edge_id)
                added_nodes.append(con_node)
            except IndexError:
                for edge_id in added_edge_ids:
                    self.remove_edge(edge_id)
                raise IndexError("ended up in here")
                return []

        return added_edge_ids, added_nodes

    def add_edge(self, edge):
        """ Make an edge known to the graph """
        self._edges[edge.edge_id].add(edge)
        edge.node1.add_neighbor(edge.node2.node_id)
        edge.node2.add_neighbor(edge.node1.node_id)

    def remove_edge(self, edge_id):
        edge = list(self._edges[edge_id])[0]
        node1, node2 = edge.node1, edge.node2
        node1.remove_neighbor(node2.node_id)
        node2.remove_neighbor(node1.node_id)
        _ = self._edges.pop(edge_id)

    def edge_exists_already(self, edge):
        for edge_in_graph in self.edges:
            if edge_in_graph.is_same_as(edge):
                return True
        return False

    def node_exists_already(self, node, return_node=True):
        for node_in_graph in self.nodes:
            # first check position, then check memory address
            if node.is_same_as(node_in_graph) and not id(node_in_graph) == id(node):
                if return_node:
                    return node_in_graph
                return True
        return False

    def straight_edge_overlaps(self, new_edge, edges, nodes):
        # edge to investigate
        p1 = new_edge.node1.position
        q1 = new_edge.node2.position

        # TODO: this creates O(N^2)!!!
        # optimizable if max length edge is known
        # use edges_to_consider() function in that case to parse edges
        for ngb_edge in edges:
            if ngb_edge == new_edge:
                continue
            p2 = ngb_edge.node1.position
            q2 = ngb_edge.node2.position
            if self.pbc:
                q1 = p1 + new_edge.v
                q2 = p2 + ngb_edge.v

            if do_intersect(p1, q1, p2, q2):
                return True
            # check if q1/q2 cross pbcs:
            if self.pbc:
                q1_outside_pbcs = p_outside_pbcs(q1, self.x_size, self.y_size)
                if q1_outside_pbcs:
                    q1_pbc = wrap_point_in_pbc(
                        q1[0], q1[1], cell=self.cell, pbc=self.pbc)
                    v1_pbc = q1_pbc - q1
                    p1_pbc = p1 + v1_pbc
                    if do_intersect(p1_pbc, q1_pbc, p2, q2):
                        return True

                q2_outside_pbcs = p_outside_pbcs(q2, self.x_size, self.y_size)
                if q2_outside_pbcs:
                    q2_pbc = wrap_point_in_pbc(
                        q2[0], q2[1], cell=self.cell, pbc=self.pbc)
                    v2_pbc = q2_pbc - q2
                    p2_pbc = p2 + v2_pbc
                    if do_intersect(p1, q1, p2_pbc, q2_pbc):
                        return True

                # TODO: this should be enough or is
                # do_intersect(p1_pbc, q1_pbc, p2_pbc, q2_pbc) needed, i.e. code below?
                if q1_outside_pbcs and q2_outside_pbcs:
                    if do_intersect(p1_pbc, q1_pbc, p2_pbc, q2_pbc):
                        return True

        # also need to check if it overlaps with a node
        if self.straight_edge_overlaps_with_nodes(
                new_edge, nodes, eps=self.node_size):
            return True

        return False

    def bend_edges(self, A: float = 0.49) -> None:
        """ Bend edges according to level of intercalation """
        for edge in self.edges:
            self.bend_edge(edge, A=A)

    def bend_edge(self, edge, A: float = 0.49) -> None:
        if hasattr(edge, 'shape'):
            return
        
        if edge.node1.rot_direction == edge.node2.rot_direction:
            # print("Frustrated edge")
            edge.shape = SineWave(
                origin=edge.node1.position,  # same as rotation direction
                p2=edge.node2.position,
                rotation_dir=edge.node1.rot_direction,
                A=A * edge.length,
                eps_discretize=self.eps_edge_overlaps,
                pbc=self.pbc,
                cell=self.cell,
                v=edge.v)
        else:
            edge.shape = SineWaveHalf(
                origin=edge.node1.position,  # same as rotation direction
                p2=edge.node2.position,
                rotation_dir=edge.node1.rot_direction,
                A=A * edge.length,
                eps_discretize=self.eps_edge_overlaps,
                pbc=self.pbc,
                cell=self.cell,
                v=edge.v)
        trimmed_line = edge.shape.trim(
            line=edge.shape.values,
            to_trim_l=edge.node1.shape.trim_length,
            to_trim_r=edge.node2.shape.trim_length)
        edge.shape.values = trimmed_line

    def new_bended_edge_overlaps(self, new_edge):
        edges = [edge for edge in self.edges
                 if hasattr(edge, 'shape') and edge != new_edge]
        edges = self.edges_to_consider_for_overlap(edges, new_edge)

        # first check overlap with any node
        node_positions = np.array([n.position for n in self.nodes], dtype=np.double)
        eps = self.node_size  # assuming cirlce radius and same size of each node
        if self.bended_edge_overlaps_with_node(new_edge, node_positions, eps):
            return True

        # second check overlap with neighboring bended edges
        if self.bended_edge_overlaps_with_bended_edge(
                new_edge, edges, self.eps_edge_overlaps):
            return True
        return False

    @property
    def cut_off_edges(self) -> float:
        """ Take 50% more to avoid missing close-by edges """
        return self.max_edge_length * 1.5

    def edges_to_consider_for_overlap(self, edges, new_edge):
        all_node_pos = np.array([node.position for node in self.nodes])
        ne_node_1_pos = new_edge.node1.position
        ne_node_2_pos = new_edge.node2.position

        ngb_nodes_1 = get_nearest_neighbors(
            ne_node_1_pos, all_node_pos, self.cut_off_edges, self.cell, self.pbc)
        ngb_nodes_2 = get_nearest_neighbors(
            ne_node_2_pos, all_node_pos, self.cut_off_edges, self.cell, self.pbc)

        ngb_nodes = list(set(np.hstack((ngb_nodes_1.flatten(),
                                        ngb_nodes_2.flatten()))))

        edges = [edge for edge in edges
                 if edge.node1.node_id in ngb_nodes
                 or edge.node2.node_id in ngb_nodes]
        return edges

    @staticmethod
    def bended_edge_overlaps_with_node(new_edge,
                                       node_positions: np.ndarray,
                                       eps: float):
        """ Only considering another node """
        # using the function more as a "points-intersect" way
        intersect = lines_intersect(
            line1=new_edge.shape.values,
            line2=node_positions,
            eps=eps)
        return intersect

    def bended_edges_overlap_with_nodes(self, edges, node_positions, eps):
        """ Node_positions can be multiple """
        for edge in edges:
            if self.bended_edge_overlaps_with_node(
                    edge, node_positions, eps=eps):
                return True
        return False

    @staticmethod
    def bended_edge_overlaps_with_bended_edge(new_edge, edges, eps=1e-2):
        """ Only considering another bended edge """
        for edge in edges:
            intersect = lines_intersect(
                line1=new_edge.shape.values,
                line2=edge.shape.values,
                eps=eps)
            if intersect:
                return True
        return False

    def straight_edges_overlap_with_node(self, edges, node_position, eps):
        for edge in edges:
            if self.straight_edge_overlaps_with_node(edge, node_position, eps):
                return True
        return False

    def straight_edge_overlaps_with_nodes(self, edge, nodes, eps):
        for node in nodes:
            if node.is_same_as(edge.node1) or node.is_same_as(edge.node2):
                continue
            node_position = node.position
            if self.straight_edge_overlaps_with_node(edge, node_position, eps):
                return True
        return False

    def straight_edge_overlaps_with_node(self, edge, node_position, eps):
        l1 = edge.node1.position
        l2 = edge.node2.position
        p = node_position
        dist = dist_p_to_straight_line(p, l1, l2, self.cell, self.pbc)
        if dist < eps:
            return True
        return False

    def assign_shape_to_node(self, node):
        shape = class_from_module('metabatt.shapes', self.node_shape)
        node.shape = shape(self.node_size)

    def assign_rotation_to_nodes(self):
        connected_nodes = self.get_connected_nodes()
        while not all([node.rotated for node in connected_nodes]):
            source_node = [
                node for node in connected_nodes if not node.rotated][0]
            assign_rotation_BFS(source_node, self)

    def add_frustrated_edges(self, tmp_node_size=0.01, tmp_eps=1e-3):
        connected_edges = []
        for edge in self.edges:
            connected_edges.append((edge.node1, edge.node2))

        original_node_size = self.node_size
        original_eps_edge_overlaps = self.eps_edge_overlaps
        self.node_size=tmp_node_size
        self.eps_edge_overlaps=tmp_eps

        connections_to_test = []
        # Not very fast but only done once at the end
        for node1 in self.nodes:
            for node2 in self.nodes:
                if node1 == node2:
                    continue
                if node1.rot_direction != node2.rot_direction:
                    continue
                if (node1, node2) in connected_edges or (node2, node1) in connected_edges:
                    # print("Connection already exists")
                    continue
                if (node1, node2) in connections_to_test or (node2, node1) in connections_to_test:
                    # print("Already added")
                    continue
                connections_to_test.append((node1, node2))

        non_overlapping_connections = []
        for connection in connections_to_test:
            edge = Edge(connection[0], connection[1], self)

            if self.straight_edge_overlaps(edge, self.edges, self.nodes):
                continue

            if self.straight_edge_overlaps_with_nodes(edge, self.nodes, self.eps_edge_overlaps):
                continue

            self.bend_edge(edge)
            if self.new_bended_edge_overlaps(edge):
                continue

            #print(f"Not overlap! {connection}.")
            non_overlapping_connections.append(connection)
            self.add_edge(edge)

        #print(f"Finished {len(non_overlapping_connections)}/{len(connections_to_test)}")

        self.node_size = original_node_size
        self.eps_edge_overlaps = original_eps_edge_overlaps

    def plot(self, node_txt: str = 'rot_direction', figsize=None):
        fig, ax = plt.subplots()

        # draw pbc lines
        if self.pbc:
            cell = self.cell
            kwargs = {'ls': '--', 'color': 'grey'}
            ax.plot([0, cell[0][0]], [0, cell[0][1]], **kwargs)
            ax.plot([0, cell[1][0]], [0, cell[1][1]], **kwargs)
            ax.plot([cell[1][0], cell[0][0]], [
                     cell[1][1], cell[1][1]], **kwargs)
            ax.plot([cell[0][0], cell[0][0]], [
                     cell[1][1], cell[0][1]], **kwargs)

        # begin by drawing all straight and bended edges
        for edge in self.edges:
            p1 = edge.node1.position
            if self.pbc:
                p2 = p1 + edge.v
                #p1, v = p_translated_pbcs(p1, p2, self, trans_v=True)
                # p2 += v #p_translated_pbcs(p2, p1, self)

                # now find crossing of vector with cell vectors
                cell_o = np.array([0, 0])
                cell_edge_points = [
                    cell_o,  # origin
                    cell_o + self.cell[0],
                    cell_o + self.cell[0] + self.cell[1],
                    cell_o + self.cell[1],
                ]

                ps_intersect = []
                p2_on_pbc = False
                for edge_inds in list(zip(list(range(4)), list(np.roll(range(4), -1)))):
                    c1 = cell_edge_points[edge_inds[0]]
                    c2 = cell_edge_points[edge_inds[1]]
                    if do_intersect(p1, p2, c1, c2):
                        p_intersect = find_intersection_line(p1, p2, c1, c2)
                        if np.allclose(p2, p_intersect):
                            p2_on_pbc = True
                            # special case if point is right on PBC line
                            pass
                        else:
                            ps_intersect.append(p_intersect)
                # connect points, but need to sort first I think
                dists = []
                for p_intersect in ps_intersect:
                    dist = p_to_p_distance(
                        p1, p_intersect, self.cell, self.pbc)
                    dists.append(dist)
                # sort to draw correct lines
                pbc_points = []
                if len(dists) > 0:
                    sorted_p = sorted(list(zip(dists, ps_intersect)))
                    pbc_points = [entry[1] for entry in sorted_p]
                points_to_draw = [p1] + pbc_points + [p2]
                n_lines = len(points_to_draw)
                line_pairs = list(zip(range(n_lines)[:-1],
                                      np.roll(range(3), -1)[:-1]))

                if not p2_on_pbc:
                    points_to_draw = wrap_points_in_pbc(
                        points_to_draw, self.cell, self.pbc)
                # if there is a pbc I need to find point RIGHT before pbc
                p_after_pbc = []
                if len(points_to_draw) > 2:
                    for p, q in line_pairs:
                        p1 = points_to_draw[p]
                        p2 = points_to_draw[q]
                        v, _ = p_to_p_distance(
                            p1, p2, self.cell, self.pbc, return_vec=True)
                        v = v[:2]
                        
                        # get correct point from before, hard coded 0.001% dist to pbc
                        if len(p_after_pbc) > 0:
                            p1 = p_after_pbc
                        p_bef_pbc = p1 + 0.999*v
                        p_after_pbc = p1 + 1.001*v
                        p1 = p1 + 0.001*v
                        p_bef_pbc, p_after_pbc, p1 = wrap_points_in_pbc(
                            [p_bef_pbc, p_after_pbc, p1], self.cell, self.pbc)
                        p2 = p_bef_pbc
                        #print(f"{p_bef_pbc=} {p_after_pbc=}")
                        #print(f"[{p1[0]:.3f}, {p1[1]:.3f}] [{p2[0]:.3f}, {p2[1]:.3f}]")

                        ax.plot([p1[0], p2[0]],
                                 [p1[1], p2[1]],
                                 color='black')

                    # for p, q in line_pairs:
                    #     p1 = points_to_draw[p]
                    #     p2 = points_to_draw[q]
                    #     ax.plot([p1[0], p2[0]],
                    #              [p1[1], p2[1]],
                    #              color='black')
                else:
                    if p_outside_pbcs(p2, self.x_size, self.y_size):
                        p2_temp = p2
                        p2 = wrap_points_in_pbc([p2], self.cell, self.pbc)[0]
                        v = p2 - p2_temp
                        p1 = p1 + v

                    ax.plot([p1[0], p2[0]],
                             [p1[1], p2[1]],
                             color='black')
            else:
                p2 = edge.node2.position
                ax.plot([p1[0], p2[0]],
                         [p1[1], p2[1]],
                         color='black')
            if hasattr(edge, 'shape'):
                buckled_line = edge.shape.values
                if self.pbc:
                    dists = np.linalg.norm(
                        buckled_line[:-1] - buckled_line[1:], axis=1)
                    cut = np.argwhere(dists > 5e-2)
                    if cut.size > 0:
                        inds = [1] + cut.flatten().tolist() + [-2]
                        tuples = [(inds[i], inds[i+1])
                                  for i in range(len(inds)-1)]
                        for start, end in tuples:
                            ax.plot(buckled_line[start+1:end-1, 0],
                                     buckled_line[start+1:end-1, 1],
                                     color='orange')

                    else:
                        ax.plot(buckled_line[:, 0],
                                 buckled_line[:, 1],
                                 color='orange')
                else:
                    ax.plot(buckled_line[:, 0],
                             buckled_line[:, 1],
                             color='orange')

        # get positions of nodes
        # positions = np.array([n.position for n in self.nodes])
        # node_colors = []

        # if len(positions) > 0:
        #     ax.scatter(positions[:, 0], positions[:, 1], s=15**2, c=node_colors)

        node_groups = [[], []]
        for node in self.nodes:
            if node.rot_direction == 'L':
                node_groups[0].append([node.position[0], node.position[1]])
            else:
                node_groups[1].append([node.position[0], node.position[1]])
        
        node_groups = [np.array(node_groups[0]), np.array(node_groups[1])]

        legend = ['L', 'R']
        colors = ['red', 'blue']

        if node_groups:
            for i, positions in enumerate(node_groups):
                if len(positions) > 0:
                    ax.scatter(positions[:, 0], positions[:, 1], s=80, label=legend[i], c=colors[i])
            ax.legend(loc='upper right')

        ax.xaxis.set_major_locator(MultipleLocator(.5))
        ax.xaxis.set_minor_locator(MultipleLocator(.25))
        ax.yaxis.set_major_locator(MultipleLocator(.5))
        ax.yaxis.set_minor_locator(MultipleLocator(.25))

        ax.set_aspect('equal')
        plt.show()

    def get_graph_score(self):
        area = self.x_size * self.y_size
        score = self.total_edge_length() / area
        return score


class Node:
    """ Basic Node in a graph """

    def __init__(self,
                 node_id: int,
                 position: Sequence,
                 rotated: bool = False,
                 rot_direction: str = None):
        self.node_id = node_id
        self.position = position
        self.rotated = rotated
        self.rot_direction = rot_direction
        self._neighbors = []

    @property
    def neighbors(self):
        """ List of neighbors IDs """
        return self._neighbors

    def add_neighbor(self, new_ngb):
        if new_ngb not in self._neighbors:
            self._neighbors.append(new_ngb)

    def remove_neighbor(self, ngb):
        if ngb in self._neighbors:
            self._neighbors.remove(ngb)

    def is_same_as(self, other):
        """ For now stay away from __eq__ """
        return np.allclose(self.position, other.position)

    def rotate(self, direction):
        assert direction in ['R', 'L']
        if not self.rotated:
            self.rot_direction = direction
            self.rotated = True

    def get_valid_rotation_direction(self, nodes):
        ngb_nodes = [node for node in nodes
                     if node.node_id in self.neighbors and node.rotated]
        ngb_rotations = [node.rot_direction for node in ngb_nodes]

        # all neighbors should have same direction
        assert len(list(set(ngb_rotations))) == 1

        if ngb_rotations[0] == 'R':
            valid_rotation = 'L'
        else:
            valid_rotation = 'R'
        return valid_rotation

    def rotate_valid_direction(self, nodes):
        valid_rotation = self.get_valid_rotation_direction(nodes)
        self.rotate(valid_rotation)

    def __repr__(self) -> str:
        return f"({self.node_id}) => [{self.position[0]:.2f}, {self.position[1]:.2f}]"


class Edge:
    """ Basic edge in the graph """

    def __init__(self,
                 node1,
                 node2,
                 graph,
                 v: np.ndarray = None):  # provide vector to avoid ambiguity for pbcs
        self.node1 = node1
        self.node2 = node2
        self.graph = graph
        self.unit_v = np.array([1, 0])  # rotation angle with respect to it
        if v is None:
            if self.graph.pbc:
                v, _ = p_to_p_distance(
                    self.node1.position, self.node2.position,
                    cell=self.graph.cell, pbc=self.graph.pbc, return_vec=True)
                v = v[:2]
            else:
                v = self.node1.position - self.node2.position
        self.v = v
        self.edge_id = (self.node1.node_id, self.node2.node_id)

        if hasattr(self, 'shape'):
            length = self.shape.length
            print(f"Has attribute! {length=}")
        else:
            p1 = np.array(self.node1.position)
            p2 = np.array(self.node2.position)
            length = round(p_to_p_distance(p1, p2, cell=self.graph.cell, pbc=self.graph.pbc)[0], 2)
            # self.length = p_to_p_distance(p1, p2, cell=self.graph.cell, pbc=self.graph.pbc)[0]
            self.length = length
            # print(f"{length=}")

    # @property
    # def length(self):
    #     if hasattr(self, 'shape'):
    #         length = self.shape.length
    #     else:
    #         p1 = np.array(self.node1.position)
    #         p2 = np.array(self.node2.position)
    #         length = p_to_p_distance(
    #             p1, p2, cell=self.graph.cell, pbc=self.graph.pbc)[0]
    #     return length

    @property
    def edge_id(self):
        return self._edge_id

    @edge_id.setter
    def edge_id(self, node_ids):
        n1_id, n2_id = node_ids
        if n2_id < n1_id:
            id1 = n2_id
            id2 = n1_id
        else:
            id1 = n1_id
            id2 = n2_id

        self._edge_id = f'{id1}-{id2}'
        # This does not mean edges are not identical!
        if self.edge_id_exists_already(self._edge_id):
            self.increment_existing_edge_id()

    def edge_id_exists_already(self, edge_id):
        if edge_id in self.graph._edges.keys():
            return True
        return False

    def increment_existing_edge_id(self):
        id1, id2 = self.edge_id.split('-')
        edge_id = f'{id2}-{id1}'
        edge_ids = self.graph._edges.keys()
        edge_ids = [eid for eid in edge_ids if eid.startswith(edge_id)]
        inc_nums = [int(eid.split('_')[1])
                    for eid in edge_ids if len(eid.split('_')) == 2]
        if len(inc_nums) == 0:
            inc_num = 1
        else:
            inc_num = max(inc_nums) + 1
        edge_id += f'_{inc_num}'

        if edge_id in self.graph._edges.keys():
            raise KeyError(f"{edge_id} edge exists already!")
        self._edge_id = edge_id

    def is_same_as(self, new_edge, eps=1e-6):
        """ Does edge already exist in graph? """
        is_same = False
        if self.graph.pbc:
            o_same = p_to_p_distance(
                new_edge.node1.position, self.node1.position,
                self.graph.cell, self.graph.pbc) < eps
            if o_same:
                v_same = np.allclose(new_edge.v, self.v)
                if v_same:
                    return True

            o_same = p_to_p_distance(
                new_edge.node1.position, self.node2.position,
                self.graph.cell, self.graph.pbc) < eps
            if o_same:
                # flip vector
                v_same = np.allclose(new_edge.v * (-1), self.v)
                if v_same:
                    return True

            if self.edge_id_exists_already(new_edge.edge_id):
                new_edge.increment_existing_edge_id()
        else:
            raise NotImplementedError("Cant compare in non-periodic case yet")

        return is_same
    def __repr__(self) -> str:
        return f"[({self.edge_id})({self.node1.position[0]:.2f}, {self.node1.position[1]:.2f}) => ({self.node2.position[0]:.2f}, {self.node2.position[1]:.2f})]"


class Subgraphs:
    """ Subgraphs that can be attached to nodes """

    def __init__(self,
                 angles: List[float],
                 lengths: List[float],
                 node=None,
                 x: float = 0,
                 y: float = 0,
                 max_connections: int = 0):
        self.angles = angles
        self.lengths = lengths
        self.node = node
        if self.node:
            x, y = node.position
        self.x = x
        self.y = y
        self.max_connections = max_connections
        self.graphs = self.get_subgraphs(self.angles, self.lengths)

    def __len__(self):
        return len(self.graphs)

    def get_subgraphs(self, angles, lengths):
        subgraphs = []
        id_ = 0
        for length in lengths:
            for angle in angles:
                subgraph = self.get_subgraph(angle, length)
                subgraph.id = id_
                subgraphs.append(subgraph)
                id_ += 1
        return subgraphs

    def get_subgraph(self, angle, length):
        subgraph = nx.Graph()
        positions = self.get_subgraph_positions(angle, length)
        for i, pos in enumerate(positions):
            subgraph.add_node(i, pos=pos)
        # add edge, currently only one edge
        assert len(positions) == 2
        subgraph.add_edge(0, 1)
        return subgraph

    def get_subgraph_positions(self, angle, length):
        """ Find position of second node given angle and length """
        x1 = length * np.cos(np.deg2rad(angle)) + self.x
        y1 = length * np.sin(np.deg2rad(angle)) + self.y
        return [[self.x, self.y],
                [x1, y1]]

    def visualize(self, delta=0.25):
        """ Visualize what the action does locally """
        M = 4
        N = int(np.ceil(len(self.graphs)/M))
        fig, axes = plt.subplots(N, M, figsize=(12, 12))
        max_length = max(self.lengths)
        for n_col in range(1, M + 1):
            for n_row in range(1, N + 1):
                i = (n_row - 1)*M + n_col - 1
                if i >= len(self.graphs):
                    if N == 1:
                        fig.delaxes(axes[n_col - 1])
                    else:
                        fig.delaxes(axes[n_row - 1, n_col - 1])
                    break
                if N == 1:
                    ax = axes[n_col - 1]
                else:
                    ax = axes[n_row - 1, n_col - 1]
                # start with axes in the middle
                ax.plot([-max_length, max_length], [0, 0],
                        '-', alpha=0.7, lw=2, color='grey')
                ax.plot([0, 0], [max_length, -max_length],
                        '-', alpha=0.7, lw=2, color='grey')
                subgraph = self.graphs[i]
                nx.draw(subgraph,
                        pos=nx.get_node_attributes(subgraph, 'pos'),
                        ax=ax)
                ax.set_xlim([-max_length - delta, max_length + delta])
                ax.set_ylim([-max_length - delta, max_length + delta])
                ax.set_aspect('equal')
                ax.get_yaxis().set_visible(True)
                ax.get_xaxis().set_visible(True)
                ax.set_title(i)
