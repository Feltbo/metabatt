#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jun  9 11:35:18 2021

@author: felix
"""
from time import time
import math
import numpy as np

def class_from_module(module, class_name):
    mod = __import__(module, fromlist=[class_name])
    class_ = getattr(mod, class_name)
    return class_


def time_function(func):
    """ Quick check - for more accurate testing use e.g. timeit """
    @wraps(func)
    def wrap(*args, **kw):
        start = time()
        func(*args, **kw)
        end = time()
        print("time: ", str(end - start), "for ", func.__name__)
    return wrap

def calculate_beam_lengths(cell_size, original_angles):
    x_cell, y_cell = cell_size
    lengths = []
    angles = []
    sin_factors = []
    cos_factors = []
    

    for angle in original_angles:
        rad_angle = math.radians(angle)
        if (angle == 0 or angle == 90 or angle == 180):
            continue

        sin_factor = round_up_to_even(y_cell / np.sin(rad_angle))
        sin_length = abs(y_cell / sin_factor)
        sin_factors.append(sin_factor)

        #print(f"Angle {angle} Sinus {np.sin(rad_angle):.4f} \nSIN: factor {sin_factor:.4f} new sin length {sin_length:.4f}")

        cos_factor = round_up_to_even(x_cell / np.cos(rad_angle))
        cos_length = abs(x_cell / cos_factor)
        cos_factors.append(cos_factor)
        
        #print(f" COS: factor {cos_factor:.4f} new cos length {cos_length:.4f}")
        
        length = np.sqrt(sin_length**2 + cos_length**2)
        if (angle > 45):
            angle = 90 - math.degrees(math.acos(1/length))
        else:
            angle = math.degrees(math.acos(1/length))

        #print(f"{length=} and {angle=}")
        
        lengths.append(length)
        angles.append(angle)
        angles.append(180 - angle)
        
    # Add straight beams
    if 0 in original_angles:
        lengths.extend(get_straight_lengths(x_cell, cos_factors))
        angles.append(0)
        
    if 90 in original_angles:
        lengths.extend(get_straight_lengths(y_cell, sin_factors))
        angles.append(90)

    if 180 in original_angles:
        angles.append(180)
        
    lengths = list(set(lengths))
    angles = list(set(angles))

    lengths.sort()
    lengths = [x for x in lengths if x > math.sqrt(2)/2]
    angles.sort()
    #print(f"{lengths=}")

    return lengths, angles

# Source: https://stackoverflow.com/questions/25361757/python-2-7-round-a-float-up-to-next-even-number
def round_up_to_even(f):
    if f % 2 == 0:
        return f
    return round(f / 2.) * 2

def get_straight_lengths(cell_size, factors):
    lengths = []
    for factor in factors:
        lengths.append(abs(cell_size / factor))
    return list(set(lengths))

def get_input_count(angles):
    return len(angles) * 2 + 2
