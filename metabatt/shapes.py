"""
Different shapes for edges and nodes
"""
import numpy as np

from metabatt.geometry import rotate_line,\
    rotate_point,\
    angle_between,\
    normalize,\
    p_to_p_distance,\
    wrap_points_in_pbc


class LineShape:

    def __init__(self, origin, p2, rotation_dir=None, pbc=None, cell=None, v=None):
        self.pbc = pbc
        self.cell = cell
        self.origin = np.array(origin)  # origin node equals rotation direction
        self.p2 = np.array(p2)
        # if/else below is redundant to what is done in Edge
        # TODO: probably LineShape should just be a subclass of an Edge
        assert v is not None
        self.v = v  # vector pointing from origin to p2
        # if rotation_dir == 'R':
        #     self.v = self.v * (-1)
        self.rotation_dir = rotation_dir
        self.unit_v = np.array([1, 0])  # rotation angle with respect to it
        self.eps = 1e-6

    @property
    def bird_distance(self):
        return np.linalg.norm(self.v)

    def trim(self, line, to_trim_l, to_trim_r):
        """ Trim a line to remove overlap with node shape """
        line_o1 = line[0]
        for i, point in enumerate(line):
            if self.pbc:
                dist = p_to_p_distance(
                    line_o1, point, cell=self.cell, pbc=self.pbc)
            else:
                dist = np.linalg.norm(point - line_o1)
            if dist > to_trim_l:
                trim_left = i
                break
        line_o2 = line[-1]
        for i, point in enumerate(line[::-1]):
            if self.pbc:
                dist = p_to_p_distance(
                    line_o2, point, cell=self.cell, pbc=self.pbc)
            else:
                dist = np.linalg.norm(point - line_o2)
            if dist > to_trim_r:
                trim_right = -i
                break
        return line[trim_left:trim_right]

    def get_rotation_angle_line(self):
        # line always defined along axis by default
        angle = angle_between(self.unit_v, self.v)
        # check if rotation > 180 deg
        # do this by rotatin default line translated by origin
        point = np.array([self.origin[0] + self.bird_distance, self.origin[1]])
        # rotate point on default axis to see if it matches p2
        rot_p = rotate_point(origin=self.origin, point=point, angle=angle)
        # include pbcs here - just important if they overlap or not
        if self.pbc:
            dist1 = p_to_p_distance(rot_p, self.p2, self.cell, self.pbc)
            dist2 = p_to_p_distance(rot_p, self.origin, self.cell, self.pbc)
        else:
            dist1 = np.linalg.norm(rot_p - self.p2)
            dist2 = np.linalg.norm(rot_p - self.origin)
        # if they do not overlap, the angle is larger than 180 deg
        if not dist1 < self.eps or dist2 < self.eps:
            angle = np.deg2rad(360 - np.rad2deg(angle))
        return angle

    def rotate_line(self, line):
        angle = self.get_rotation_angle_line()
        # now rotate with correct angle
        rot_line = rotate_line(self.origin, line, angle)
        return np.array(rot_line)

    def mirror_line(self, line, mirror_vector=np.array([1, -1])):
        """ Mirror a set of points along a vector """
        return np.array(line) * mirror_vector


class SineWaveHalf(LineShape):
    def __init__(self, A, eps_discretize, *args, **kwargs):
        super(SineWaveHalf, self).__init__(*args, **kwargs)
        self.A = A
        self.eps_discretize = eps_discretize
        self._values = self.sine_wave_half()

    @property
    def values(self):
        return self._values

    @values.setter
    def values(self, line):
        """ Wrap points directly into pbc """
        if self.pbc:
            line = wrap_points_in_pbc(line, cell=self.cell, pbc=self.pbc)
        self._values = line

    def sine_wave_half(self):
        # find interpolation based on length
        # for a square, the minimum distance is 0.707*eps
        # pure epsilon is okay, but done for straight line!
        eps = self.eps_discretize
        interpolate = int(self.bird_distance/eps)
        # normalize to pi for arch shape
        omega = np.pi/self.bird_distance
        x = np.linspace(0, self.bird_distance, interpolate)
        line = np.vstack((x, self.sine_wave(self.A, omega, x))).T
        self.length = line
        assert self.rotation_dir is not None  # direction needs to be known
        if self.rotation_dir == 'R':
            # by default it is defined as 'L'
            line = self.mirror_line(line)
        rot_line = self.rotate_line(line)
        self.values = rot_line
        return rot_line

    def sine_wave(self, A, omega, x):
        return A*np.sin(omega*x)

    @property
    def length(self):
        return self._length

    @length.setter
    def length(self, line):
        """ Calculate length before wrapped into pbc """
        _length = sum(np.linalg.norm(line[:-1] - line[1:], axis=1))
        self._length = _length

class SineWave(LineShape):
    def __init__(self, A, eps_discretize, *args, **kwargs):
        super(SineWave, self).__init__(*args, **kwargs)
        self.A = A
        self.eps_discretize = eps_discretize
        self._values = self.sine_wave_points()

    @property
    def values(self):
        return self._values

    @values.setter
    def values(self, line):
        """ Wrap points directly into pbc """
        if self.pbc:
            line = wrap_points_in_pbc(line, cell=self.cell, pbc=self.pbc)
        self._values = line

    def sine_wave_points(self):
        # find interpolation based on length
        # for a square, the minimum distance is 0.707*eps
        # pure epsilon is okay, but done for straight line!
        eps = self.eps_discretize
        interpolate = int(self.bird_distance/eps)
        # normalize to pi for arch shape
        omega = 2*np.pi/self.bird_distance
        x = np.linspace(0, self.bird_distance, interpolate)
        line = np.vstack((x, self.sine_wave(self.A/2, omega, x))).T
        self.length = line
        assert self.rotation_dir is not None  # direction needs to be known
        if self.rotation_dir == 'R':
            # by default it is defined as 'L'
            line = self.mirror_line(line)
        rot_line = self.rotate_line(line)
        self.values = rot_line
        return rot_line
        pass

    def sine_wave(self, A, omega, x):
        return A*np.sin(omega*x)

    @property
    def length(self):
        return self._length

    @length.setter
    def length(self, line):
        """ Calculate length before wrapped into pbc """
        _length = sum(np.linalg.norm(line[:-1] - line[1:], axis=1))
        self._length = _length

class BodyShape:
    def __init__(self):
        pass


class Circle(BodyShape):
    """ Classic round circle for node shape """

    def __init__(self, radius, *args, **kwargs):
        super(Circle, self).__init__(*args, **kwargs)
        self.radius = radius

    @property
    def trim_length(self):
        return self.radius


if __name__ == "__main__":
    if 0:
        import matplotlib.pyplot as plt
        p2 = np.array([5, -5.2])
        p1 = np.array([1, 1])
        line_rot = SineWaveHalf(origin=p2, p2=p1, rotation_dir='R',
                                A=3, omega=2).sine_wave_half()
        plt.plot([p1[0], p2[0]], [p1[1], p2[1]])
        plt.plot(line_rot[:, 0], line_rot[:, 1])
        ax = plt.gca()
        ax.set_aspect('equal')
        plt.show()
