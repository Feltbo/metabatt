import numpy as np

import metabatt
from metabatt.graph import Graph, Subgraphs
from metabatt.reinforce.environments import MetabattEnvSubgraphs
from metabatt.reinforce.agents import QAgentReplayMemory
import matplotlib.pyplot as plt


    
def test_subgraphs():
    sg = Subgraphs([0, 15, 45, 90, 135, 180], [1])
    sg.visualize()

def test_agent_generator(monkeypatch):
    monkeypatch.setattr(plt, 'show', lambda: None)
    subgraphs = Subgraphs(angles=[0, 30],
                              lengths=[1])
    # create the environment
    env = MetabattEnvSubgraphs(n_cells_x=3,
                               n_cells_y=3,
                               subgraphs=subgraphs)
    
    agent = QAgentReplayMemory(env=env,
                              n_inputs=7, # this is the state representation
                              n_outputs=env.action_space.n,
                              max_epsilon=1.0,
                              min_epsilon=0.05,
                              gamma=0.99,  # discounted reward
                              tau=0.01, # target network update rate
                              batch_size=8,  # Experience replay, trained agent uses 64
                              lambda_=-np.log(0.01)/10,  # decay for epsilon-greedy
                              replay_memory_capacity=16,  # trained agent uses 1000
                              learning_rate=0.0005,
                              prefill_memory=True,
                              random_actions=False) # random agent
    # train for one single episode
    agent.train(num_episodes=1)
    # check that some data is present
    rewards = agent.rewards


def test_agent_load_and_predict(monkeypatch):
    monkeypatch.setattr(plt, 'show', lambda: None)
    # get agent
    # we start from scratch here
    subgraphs = Subgraphs(angles=[0, 30, 60, 90, 120, 150, 180],
                          lengths=[1])
    
    # create the environment
    env = MetabattEnvSubgraphs(n_cells_x=3,
                               n_cells_y=3,
                               subgraphs=subgraphs)
    
    NUM_EPISODES = 15  # this is very little, trained agent uses 500
    # create an agent and set hyperparameters
    agent = QAgentReplayMemory(env=env,
                              n_inputs=16, # this is the state representation
                              n_outputs=env.action_space.n,
                              max_epsilon=1.0,
                              min_epsilon=0.05,
                              gamma=0.99,  # discounted reward
                              tau=0.01, # target network update rate
                              batch_size=8,  # Experience replay, trained agent uses 64
                              lambda_=-np.log(0.01)/NUM_EPISODES,  # decay for epsilon-greedy
                              replay_memory_capacity=16,  # trained agent uses 1000
                              learning_rate=0.0005,
                              prefill_memory=True,
                              random_actions=False,  # random agent
                              save_best_graph=True)  # save the highest ranking graph state
        
    # lets try to get the static path to this file, fingers crossed
    file_name = 'pretrained_16_16'
    path_to_model = './pretrained_models/' + file_name
    # we need to get a model to load the weights into, all this is done when calling load_model
    best_model = agent.load_model(file_name=path_to_model)
    
    # now use that model to make predictions
    # note that even though epsilon is 0, the node selection is still random!
    agent.predict(model=best_model, num_episodes=1, epsilon=0, save_best_graph=True)
    
