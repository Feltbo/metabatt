#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov  1 11:44:07 2021

@author: felix
"""
from metabatt.reinforce.agents import QAgentReplayMemory

def test_edge_same_as():
    """ Test that edge same as function works """
    pass

def test_agent_load_hyperparameters():
    hyperparams_path = './pretrained_models/hyperparams'
    agent = QAgentReplayMemory.from_hyperparameters(fname=hyperparams_path)
    
    