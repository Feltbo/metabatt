# Metabat

Experimental code development for the metabat project

## Installation

Cython is required, but should be installed when running pip following:

```
git clone https://gitlab.com/Feltbo/metabatt.git
cd metabatt
pip install --editable .
```
If you want to use a virtual environment you can use
```
python3 -m venv my_env
source PATH_TO_ENV/my_env/bin/activate
```

### Use the virtual environment in a Jupyter notebook
For using the virtual environment in your jupyter notebook, you have too add the Kernel doing something like:
```
pip install --user ipykernel
python -m ipykernel install --user --name=my_env
```
You should then be able to run the notebook and specify your environment.

## Folder structure

- example_notebooks: Reference for creating new notebooks
- experiments: The experiments are divided in folders, each with a notebook consisting of one or multiple runs of the program.
- metabatt: python source code
- src: compliled source code
- tests: tests for the program
