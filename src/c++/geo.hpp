#include <vector>
#include <math.h>
using namespace std;

bool lines_intersect(double x1[],
                     double y1[],
                     double x2[],
                     double y2[],
                     int N1,
                     int N2,
                     float eps) {
    double eps_squared;
    double dist;
    
    eps_squared = eps*eps;
    for (int i = 0; i < N1; ++i) {
        for (int j = 0; j < N2; ++j) {
            dist = pow(x1[i] - x2[j], 2.0);
            if (dist < eps_squared) {
                dist += pow(y1[i] - y2[j], 2.0);
                if (dist < eps_squared) {
                    return true;
                }
            }
        }
    }
    return false;
}
