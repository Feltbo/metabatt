#!/usr/bin/python3
# distutils: language = c++
"""
Functions that need speed up
"""
from libc.math cimport pow

def lines_intersect_cpp(double[:] x1_py,
                        double[:] y1_py,
                        double[:] x2_py,
                        double[:] y2_py,
                        float eps=0.01):
    """ Two lines with line=[[x1, y1], [x2, y2], ...] intersect 
    
    Notes
    ------
    It is avoided to calculate the square root at every step. Additionally
    time is saved by only calculating one part of the norm and checking its
    value first.
    """
    cdef double[:] x1 = x1_py  # memoryview
    cdef double[:] x2 = x2_py  # memoryview
    cdef double[:] y1 = y1_py  # memoryview
    cdef double[:] y2 = y2_py  # memoryview
    cdef float dist
    cdef double eps_squared
    cdef Py_ssize_t v1_max = x1.shape[0]
    cdef Py_ssize_t v2_max = x2.shape[0]
    cdef Py_ssize_t x, y
    
    eps_squared = eps*eps
    for i in range(v1_max):
        for j in range(v2_max):
            # recalculate every time
            dist = pow(x1[i] - x2[j], 2.0)
            if dist < eps_squared:
                dist += pow(y1[i] - y2[j], 2.0)
                if dist < eps_squared:
                    return True
    return False
