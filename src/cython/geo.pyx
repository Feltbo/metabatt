# distutils: language = c++
from libcpp.vector cimport vector
from libcpp cimport bool
import numpy as np


cdef extern from "geo.hpp":
    bool lines_intersect(double *x1,
                         double *y1,
                         double *x2,
                         double *y2,
                         int N1,
                         int N2,
                         float eps)


def lines_intersect_basic_cpp(x1_py, y1_py, x2_py, y2_py, eps=1e-2):
    cdef double [::1] x1 = np.ascontiguousarray(x1_py)
    cdef double [::1] y1 = np.ascontiguousarray(x1_py)
    cdef double [::1] x2 = np.ascontiguousarray(x2_py)
    cdef double [::1] y2 = np.ascontiguousarray(x1_py)
    # parse arrays as pointers
    # https://cython.readthedocs.io/en/latest/src/userguide/memoryviews.html
    return lines_intersect(&x1[0], &y1[0], &x2[0], &y2[0], 
                           x1.shape[0], x2.shape[0], eps=eps)
