from setuptools import setup, find_packages, Extension
from Cython.Build import cythonize

extensions = [
    Extension("src.cython.geometry", ["src/cython/geometry.pyx"]),
    Extension("src.cython.geo", 
              sources = ["src/cython/geo.pyx"],
              include_dirs=["src/c++/"],
              language="c++")
]

compiler_directives = {"language_level": 3}
ext_modules = cythonize(extensions, compiler_directives=compiler_directives)


setup(
    name="metabatt",
    version="0.0.0",
    author="Felix Tim Boelle",
    author_email="feltbo@dtu.dk",
    description="Metamaterials for silicon anode materials",
    maintainer='Felix Tim Boelle',
    maintainer_email='feltbo@dtu.dk',
    url="https://gitlab.com/Feltbo/metabat",
    packages=find_packages(),
    install_requires=[
        'numpy',
        'matplotlib',
        'pytest',
        'pytest-cov',
        'networkx',
        'ase>3.21',
        'gym==0.21',
        'torch',
    ],
    ext_modules=ext_modules,
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: Unix",
    ],
    python_requires='>=3.6',
)
